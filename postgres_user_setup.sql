-- Run all this with user having admin rights (postgres user)
-- On windows, to login admin shell, go to CMD and do
-- `psql -U postgres`

-- On Linux it is slightly different as you need to do
-- `sudo -u postgres psql`

-- Then run this script with
-- `\i nameofscript.sql`

CREATE ROLE mern WITH LOGIN PASSWORD 'mern';
CREATE DATABASE express001 WITH OWNER mern;

-- Now run the migration.sql as above