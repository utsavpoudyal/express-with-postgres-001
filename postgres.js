const pg = require("pg");
const { Client } = pg;

async function dbQuery(query, userObj) {
    try {
        const dbObj = new Client(userObj);
        await dbObj.connect();
        let output = await dbObj.query(query);
        await dbObj.end();
        return output.rows;
    } catch (e) {
        console.log(e.stack);
    }
}

module.exports = dbQuery;
