-- Postgresql

CREATE TABLE users(
    id SERIAL,
    PRIMARY KEY(id),
    username VARCHAR(20) NOT NULL UNIQUE,
    userpassword VARCHAR(40) NOT NULL
);

CREATE TABLE people(
    id SERIAL references users(id) ON DELETE CASCADE,
    PRIMARY KEY(id),
    name VARCHAR(50) NOT NULL,
    dob DATE
);

CREATE TABLE numbers(
    numbers VARCHAR(15),
    PRIMARY KEY(numbers),
    people_id int REFERENCES people(id) ON DELETE CASCADE
);

CREATE TABLE categories(
id SERIAL,
PRIMARY KEY(id),
categoryname varchar(20),
parent_category int REFERENCES categories(id) ON DELETE SET NULL
);

CREATE TABLE products(
    id SERIAL,
    PRIMARY KEY(id),
    productname VARCHAR(50) NOT NULL,
    quantity int NOT NULL,
    price real NOT NULL,
    category_id int REFERENCES categories(id) ON DELETE SET NULL
);

CREATE TABLE purchase(
    user_id int REFERENCES users(id) ON DELETE CASCADE,
    product_id int REFERENCES products(id) ON DELETE CASCADE,
    PRIMARY KEY(user_id, product_id),
    purchase_date DATE DEFAULT NOW()
);