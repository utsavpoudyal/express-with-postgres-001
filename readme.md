## Steps to clone and run the project

-   Clone this repo with

```
git clone https://gitlab.com/utsavpoudyal1120/express-with-postgres-001
```

-   Install all the required packages with

```
npm i
```

-   Setup postgres with the help of `postgres_readme.md`
-   Run the project with

```
npm start
```
