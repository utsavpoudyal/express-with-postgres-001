## Setup postgres

-   Install postgres and login as admin user, Which is mostly `postgres` user

### To login as postgres user in different Operating Systems

#### Windows

-   Open up cmd and run

```
psql -U postgres -h localhost
```

#### GNU/Linux

##### Ubuntu

-   Open up Terminal and run

```
sudo -u postgres psql
```

-   In postgres shell logged in as `postgres`, run the script `postgres_user_setup.sql` with the command

```
\i postgres_user_setup.sql
```

-   Then load in the `migration.sql`, run the script `migration.sql` with the command

```
\i migration.sql
```
