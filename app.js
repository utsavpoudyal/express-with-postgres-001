const express = require("express");
const fs = require("fs");
const path = require("path");
const query = require("./postgres.js");
const port = 3000;
const userObj = JSON.parse(fs.readFileSync("./postgresUserInfo.json"));

const app = express();
app.use(express.json());

app.get("/users", async (req, res) => {
    try {
        let users = await query(
            "select * from people join users on users.id = people.id",
            userObj
        );
        users = await Promise.all(
            users.map(async (user) => {
                let numbers = await query(
                    `select numbers from numbers where numbers.people_id = ${user.id}`,
                    userObj
                );
                numbers = await numbers.map((number) => number.numbers);
                return { ...user, numbers: numbers };
            })
        );
        res.send(users);
    } catch (e) {
        console.log(e.stack);
        res.statusCode = 500;
        res.send("Error occured when reading data");
    }
});

app.post("/users", async (req, res) => {
    let data = req.body;
    /*
        {
            name: 'Admin',
            dob: '2000-10-10T18:15:00.000Z',
            username: 'admin',
            userpassword: 'password'
        }
    */
    let { name, dob, username, userpassword, numbers } = data;
    try {
        /*
        We need to put user creation and people creation under a single transaction
        so that database will treat it as a single query and if one fails, whole
        thing fails
        */
        let userAddedSuccessful = false;
        let numberAddedSuccessful = false;
        await query(
            `
            BEGIN;
                insert into users(username, userpassword) values('${username}', '${userpassword}');
                insert into people(name, dob) values('${name}', '${dob}');
            COMMIT;
        `,
            userObj
        );
        let justInsertedUser = await query(
            `select * from users where username = '${username}'`, userObj
        );
        if (justInsertedUser.length > 0) {
            userAddedSuccessful = true;
        }
        // await query(
        //     `insert into users(username, userpassword) values('${username}', '${userpassword}');`,
        //     userObj
        // );
        // await query(
        //     `insert into people(name, dob) values('${name}', '${dob}');`,
        //     userObj
        // );
        if (numbers.length > 0 && userAddedSuccessful) {
            let id = await query(
                `select id from users where username = '${username}'`,
                userObj
            );
            id = id.map((userid) => userid.id);
            id = id[0];
            console.log(id);
            console.log(typeof id);
            await numbers.forEach(async (number) => {
                console.log(number);
                await query(
                    `insert into numbers(numbers, people_id) values('${number}','${id}')`,
                    userObj
                );
            });
        }
        res.send("Data written successfully");
    } catch (e) {
        res.send(e.stack);
    }
});

app.get("/products", async (req, res) => {
    res.send(
        await query(
            "select * from products join categories on products.category_id = categories.id"
        )
    );
});

app.listen(port, () => console.log(`Server started at port ${port}`));
